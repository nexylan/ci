# Nexylan CI

Heavily inspired from [GitLab Auto DevOps][gitlab_ci_templates].

Some part of GitLab CI templates are directly used.

## Usage

On top of your `.gitlab-ci.yml`:

```yaml
include: { project: nexylan/ci, ref: vX.Y.Z, file: auto.gitlab-ci.yml }
```

Note: On CE on-premise instance, you may want to use `ce.gitlab-ci.yml` instead.

You also may:

- Add only some specific templates of the Nexylan Auto DevOps.
- Override some part of the included template.

To achieve it, please refer to the [official documentation][gitlab_ci_include_doc].

⚠️ Nexylan projects **should always** include the complete Auto DevOps template.

## Configuration (optional)

You can change the continuous integration/delivery project mode:

```yaml
variables:
  NEXY_APP_DOMAIN: ci.nexylan.com
  NEXY_CI_MODE: "node"
  NEXY_CD_MODE: "node"
  NEXY_CD_TIMEOUT: 1200
```

Just choose the mode you need and push to get
the CI feedback and adjust configuration if necessary.

- `NEXY_APP_DOMAIN`: The base domain to use for deploy environments.
- `NEXY_CI_MODE` (string):
  - docker (default)
  - node
  - none
- `NEXY_CD_MODE` (string):
  - none (default)
  - docker
  - node
  - static
- `NEXY_CD_TIMEOUT` (int): Global timeout (in seconds) for any deploy job (like docker compose up)
- `NEXY_CD_TIMEOUT` (int): Global timeout (in seconds) for any deploy job (like docker compose up)
- `NEXY_CD_SERVICES` (string): The docker compose services (e.g. `app db search`). Leave blank to deploy all services.
- `NEXY_STATIC_PATH` (string): Node version to use (deploy:static only).
- `NEXY_DOCKER_PARALLEL` (boolean): Set to `true` to enable parallel docker build. This option is recommended if your **are not** multi-layer building.
- `NEXY_DOCKER_NETWORK` (string): Docker network to use (deploy:static only).
- `NODE_VERSION` (string): Node version to use (node only).

Defaults are defined in [`ce.gitlab-ci.yml`](ce.gitlab-ci.yml).

### Stage disabling

You can manually disable some stages by setting `0` value to the following variables:

- `NEXY_CI_STAGE_BUILD`
- `NEXY_CI_STAGE_TEST`
- `NEXY_CI_STAGE_DEPLOY`
- `NEXY_CI_STAGE_RELEASE`
- `NEXY_CI_STAGE_VERIFY`

This may be useful for some specific jobs like maintenance scheduled pipelines.

### Env file alternatives

The CI variables can also be on the `.env` file:

```
NEXY_APP_DOMAIN=ci.nexylan.com
```

This method allow you more flexibility to share the variables across tools.

However, you may not want to use this method if the `NEXY_` variables are going to be used by GitLab only.

### Using docker compose

If you build your project using docker compose, you have to setup some additional configurations to well name your images.

First, define your default `IMAGE_NAME` and `IMAGE_TAG`:

```.env
IMAGE_NAME=your/project/path
IMAGE_TAG=latest
```

Those variables will be overwritten on CI.

Then, use the tags on your `docker-compose.yml` file:

```yaml
version: "3.7"

services:
  app:
    image: ${IMAGE_NAME}:${IMAGE_TAG}
    build:
      context: .
    depends_on:
      - db

  api:
    image: ${IMAGE_NAME}/api:${IMAGE_TAG}
    build:
      context: api
    depends_on:
      - db

  db:
    image: bitnami/mysql:5.7-debian-9
    environment:
      - ALLOW_EMPTY_PASSWORD=yes
```

Note: This change nothing when you are using vendor images directly.

#### Extra files

Some additional docker compose file can be added alongside the main one:

- docker-compose.ci.yml: Used on any stage when provided. Useful for CI specific usages.
- docker-compose.dev.yml: Used on build and test build stages. Useful for test only images.
- stack.yml: Used only on deploy stage. Useful to setup deploy only configuration like traefik labels.
- docker-compose.production.yml: Used on any stage under production environnement.
- docker-compose.staging.yml: Used on any stage under staging environnement.
- docker-compose.review.yml: Used on any stage under review environnement, for merge requests.

### Configure your deployment runner

In order to deploy your project, you will need to setup your custom runner.

The easy way to deploy with docker stack is to have a dedicated runner on the docker swarm manager.

See also: https://dockerswarm.rocks/gitlab-ci/

Assuming choose the tag `deploy` for your custom runner, you can use the following configuration:

```yaml
._deploy:staging: &deploy_staging
  tags:
    - project:deploy:staging

._deploy:production: &deploy_production
  tags:
    - project:deploy:production

deploy:review: *deploy_staging
deploy:review:stop: *deploy_staging
deploy:staging: *deploy_staging
deploy:production: *deploy_production
```

#### Static and Node deployments

For `static` and `node` CD mode, this project use [built-in docker configurations](images/docker/rootfs/dockerfiles).

You may want to customize the deploy docker compose configuration to pass some environnement variables.

To achieve it, create a `stack.yml` file on the root folder of your project:

```yaml
version: "3.7"

services:
  app:
    environment:
      - CUSTOM_ENV_VAR
      - ANOTHER ONE
```

## Tasks store

Nexylan/CI comes with "ready to use" independent tasks that can be added to your stack by simple import lines:

```yaml
include: { project: nexylan/ci, ref: vX.Y.Z, file: tasks/verify/jscpd.gitlab-ci.yml }
```

A tasks represent one or several jobs that are dispatched across the several available stages according to the needs.

You can browse the available task on the [`tasks` directory](./tasks).

Some tasks may be provided with a dedicated `task.md` for configuration details.

## Extra features

### Private GitLab npm registry deployment

Nexylan/CI provides a CI configuration to deploy npm package for production (release) and Merge Request.

To use it, add the following configuration to your `package.json` file:

```json
  "publishConfig": {
    "@scope:registry": "https://gitlab.com/api/v4/projects/1337/packages/npm/"
  },
```

- `scope`: Your related group slug. For example: `nexylan`.
- `1337`: Your project ID. You can find it on your project homepage.

Then, include the related template:

```yaml
include: { project: nexylan/ci, ref: vX.Y.Z, file: templates/deploy/npm.gitlab-ci.yml }

variables:
  # Optional: Default to root project directory
  NPM_PUBLISH_PATH: publish-folder
```

Production deployment job is automatically triggered and will publish a npm package corresponding to the published release number (vX.Y.Z).

There is no need to specify your scope or your project ID on the CI configuration, it will be automatically fetched using the GitLab CI environment variables.

Review deployment job has to be triggered manually on from the related Merge Request page. It will produce a package named like `0.0.0-${CI_COMMIT_SHORT_SHA}`.
This one is useful if you need to test your package integration before merging anything but is not automated to avoid releases pollution.

More information about npm package publishing: https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package

## FAQ

### I don't like one of your CI feature, how to deactivate it?

For basic CI/CD features, use the configuration variables:

```yaml
variables:
  NEXY_CI_MODE: none
  NEXY_CD_MODE: none
  # The email MUST match an authorized GitLab account.
  NEXY_BOT_EMAIL: bot@team.com
  # Default to "bot"
  NEXY_BOT_NAME: "Wonderful Bot"
```

You can also deactivate an entire stage also with variables:

```yaml
variables:
  NEXY_CI_STAGE_BUILD: 0
  NEXY_CI_STAGE_TEST: 0
  NEXY_CI_STAGE_DEPLOY: 0
  NEXY_CI_STAGE_RELEASE: 0
  NEXY_CI_STAGE_VERIFY: 0
```

Assuming you want to remove the `commit` feature, you can do this:

```yaml
.deactivate: &deactivate
  rules:
    - when: never

commit: *deactivate
# The release feature won't work if you don't respect the commit format.
# The release feature won't work if you don't respect the conventional commit format.
# More info: https://gitlab.com/nexylan/release/
release: *deactivate
```

### I would like the review deployment job to be manual

On some big projects, the deployment process might be long and resource consuming.
On that case, you may not want to deploy at every merge request for this project.

This can be done by overriding the concerned deploy job:

```yaml
._deploy:review: &deploy_review
  rules:
    - if: $NEXY_CI_STAGE_DEPLOY != "1" || $NEXY_CD_MODE == "none"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      # The real override is here.
      when: manual
      allow_failure: true

deploy:review: *deploy_review
# This has to be also set to the stop job to not break the pipeline workflow.
deploy:review:stop: *deploy_review
```

Note: This example works for merge request deployment but might be applied to any configured deployment job.

For more info about the rules: https://docs.gitlab.com/ee/ci/yaml/#rules-clauses

## Troubleshooting

### I have a bug only on the CI job, how can I reproduce it?

You may want to [run the runner locally](https://docs.gitlab.com/runner/commands/#gitlab-runner-exec),
but it could be rapidly cumbersome for your need.

You could also run your failing script on a CI like docker environment:

```
docker run -w /code -v$(pwd):/code \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e CI_REGISTRY_IMAGE=registry.gitlab.com/nexylan/n-admin/test:foo \
  -it registry.gitlab.com/nexylan/ci:x.y.z-docker bash
```

Note: Don't forget to specify all the environnement variable your script need.

The image list is available [here](https://gitlab.com/nexylan/manager/back/api/container_registry).

### The CI can't access to my repository

Here is the error you may get:

```
fatal: unable to access 'https://gitlab-ci-token:[secure]@[...]': The requested URL returned error: 403
```

You need to [create a personal access token][gitlab_doc_personal_access_token] with the following scopes:

- api
- read_repository
- write_repository

Then, add a `GITLAB_TOKEN` variable on your [project or group CI/CD settings][gitlab_doc_custom_env_var].

### I have a "invalid reference format" error on build stage

You may have an error like this:

```
ERROR: for test  invalid reference format
invalid reference format
```

Here, our faulty service is `test`.

The CI try to push an image to the registry, but does not know what name to use.

You have to specify the `image` to make it working:

```yaml
version: "3.7"

services:
  test:
    image: ${IMAGE_NAME}/test:${IMAGE_TAG}
```

### I have a "No such service: test" error on test stage

When you are using docker compose, the CI expect to run a `test` service.

The `test` service can be whatever you want. Here is a very basic implementation:

```yaml
# docker-compose.dev.yml
version: "3.7"

services:
  test:
    image: registry.gitlab.com/nexylan/docker/core:latest-alpine
    command: wait-for-it server:80 --timeout=60 --strict
```

Here, we just ensure our `server` service is up. You can implement some test suites later on your project.

### The CI try to build nexylan/image instead of my image name

This is because the `CI_REGISTRY_IMAGE` env var is not provided.

Please ensure the docker registry is enabled on your repository:

![container_registry_option](https://gitlab.com/nexylan/ci/uploads/084134057a7d587c4a0ff38fcd1c4ef2/image.png)

[gitlab_ci_include_doc]: https://docs.gitlab.com/ee/ci/yaml/#include "GitLab CI include documentation"
[gitlab_ci_templates]: https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates "CI templates list"
[gitlab_doc_custom_env_var]: https://docs.gitlab.com/ee/ci/variables/#creating-a-custom-environment-variable "Creating a custom environment variable"
[gitlab_doc_personal_access_token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token "Creating a personal access token"
