# JSCPD

A Copy/paste detector for JavaScript base programming source code.

This job will run the `jscpd` binary and fail in case of duplicate code report.

An HTML report is also attached to each CI jobs for further analysis.

## Usage

```yaml
include: { project: nexylan/ci, ref: vX.Y.Z, file: tasks/verify/jscpd.gitlab-ci.yml }
```

### Variables configuration

```yaml
variables:
  - NEXY_CI_SRC_PATH: src # The source path to analyse.
```
